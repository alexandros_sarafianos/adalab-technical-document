\chapter{ Simulating gene expression levels }
\section{Boolean network}
\subsection{Introduction}
A Boolean network is a network in which the nodes are boolean variables (i.e. they can take the values 0 and 1) and their values are updated by means of a set of boolean rules. 
Typically this is achieved by discretizing time and then updating a state at time $t$ by executing the boolean rules with the state of time $t-1$\footnote{We are currently using synchronous way, i.e. all nodes are updated simultaneously based on the values of the previous time step,
alternatives are asynchronous updates and hybrids }. The dynamics of  boolean networks have been studied in the past. Due to the fact that the values of a boolean network are 0/1, the amount
of possible states is $2^N$ with $N$ the amount of boolean variables. States to which the network evolves for different sets of initial conditions are called attractors and have been linked to
phenotypes in the past \cite{li2004yeast}. 
More formally, a boolean network simulator is a function $f$ that maps an experiment to a time series of node states. More formally: $ f: \mathbf{x} \to \{0,1\}^{N \times T}$ where $N$ is the amount of nodes in the network, $T$ the timepoints for which the values 
have to be estimated and $\mathbf{x}$ is an input vector corresponding to a biological experiment. 
\subsection{ Model for diauxic shift } \label{sec:zimmer}
As a start model, we have opted for the diauxic shift regulatory network described by \cite{geistlinger2013comprehensive}, which is the most thorough descriptive model available for diauxic shift and contains more than 300 regulatory interactions. 
Figure \ref{fig:in_edges_degree} and \ref{fig:out_edges_degree} shows information about the degrees of nodes, which follow a power-law distribution, in this model.
\begin{figure}
    \centering
    \begin{subfigure}[b]{0.4\textwidth}
        \includegraphics[width=\textwidth]{Images/in_edges_zimmer.png}
    \caption{ Amount of nodes having a certain degree (incident edges) }
    \label{fig:in_edges_degree}
    \end{subfigure}
    \begin{subfigure}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{Images/out_edges_zimmer.png}
    \caption{ Amount of nodes having a certain degree (outgoing edges) }
    \label{fig:out_edges_degree}
    \end{subfigure}
    \end{figure}
Nodes in the model are either genes, metabolites, conditions, drugs or complexes. The Zimmer model contains a node ``diauxic shift'', which the authors describe as a diauxic shift switch (a sensing node). When this diauxic shift node is activated through
value propagation, we assume that diauxic shift has occurred. \chem{O_2} is assumed to be sufficiently present in the model to be able to trigger diauxic shift. Alternatively we also checked the value of the ethanol node in the model. 

\subsection{Adam Data set}
\subsubsection{General info}
The Adam dataset contains information about which gene is knocked out, information on the experimental conditions such as growth medium and an estimation of the diauxic shift growth parameters.
We addressed the issue as a classification problem (diauxic shift occurs or not). To this purpose we assumed that diauxic shift was 
only occuring when the value \textit{diauxic\_shift\_rate} from the Adam dataset was greater than 0. Depending on which thresholds are chosen, 
different amounts of positive and negative examples can be obtained. A summary of this is shown in table \ref{tab:summary_thresh}.
\begin{table}[htb]
	\centering
   % \resizebox{0.3\textwidth}{!}
   % {
    \begin{tabular}{lllll}
    \hline
    Type&Lower threshold&Upper threshold&Positives&Negatives\\ \hline
    1 & 0.01&0.99&93&166 \\
    2 & 0.05&0.95&184&75 \\
    3 & 0.1&0.9&210&49 \\
    4 & 0.15&0.85&224&35 \\
    5 &0.2&0.8&243&16 \\
    6 &0.1&0.8&225&34 \\
    \hline
    \end{tabular}
  %  }
    \caption{ Summary of data for different thresholds. }
	\label{tab:summary_thresh}

\end{table}
\subsubsection{Genes}
For experiments with strains for which a certain gene is knocked out, the corresponding gene in the network was set to 0 and this was maintained throughout the experiment. Genes that are not present in the network are handled as if there was no gene knocked out and 
therefore give the same result as the wild strain. This was the case for 202 of the 259 strains in the dataset, leaving 58 interesting experiments if we include the wild strain once.
\subsubsection{Other experimental factors}
These experimental factors were manually selected based on the used growth medium described in the Adam data set.
\begin{itemize}
 \item Glucoseext was set to 1, not fixed throughout experiment
 \item Nitrogen was also set to 1 and not fixed
\end{itemize}
For the unspecified experimental factors, including genes, we used two different approaches:
\begin{enumerate}
 \item All unspecified factors are initialized to 0 and are allowed to change
 \item The unspecified genes are initialized with random values (motivated by the dynamics of attractors) and results are averaged over $N$ different initializations.
\end{enumerate}
% \subsection{Influential genes dataset: List of influential genes by means of rank aggregation}
% List obtained from Daniel and Mohamed, based on Derisi dataset \cite{derisi1997exploring} and Brauer dataset \cite{brauer2008coordination}. [More details]
% ``This rank gives us an idea about how influential some genes are during diauxic shift.``
% '' what we could do is, one by one (maybe just the first 10 
% TF that are part of the model, and depending on the results continue) is to knock out the TF. This is, set their state always to zero.
% I guess ideally the diauxic shift shouldn’t be present in the top knock outs. ''

\subsection{Results}
\subsubsection{Adam dataset}
The confusion matrices obtained from starting with 100 different initial gene states for the different types can be found in tables \ref{tab:adam_results_sim_type1},\ref{tab:adam_results_sim_type2},
\ref{tab:adam_results_sim_type3}, \ref{tab:adam_results_sim_type4}, \ref{tab:adam_results_sim_type5}, \ref{tab:adam_results_sim_type6}.
A summary of the predictive performance of the network for all types is shown in table \ref{tab:summary_type_results}. With the specified set of experimental factors, the wild strain also exhibited diauxic shift.
As is clearly shown in table \ref{tab:summary_type_results} the boolean model fails to predict the absence of diauxic shifts.
% (TODO: add the data about remaining 203 strains. Can be substracted from tables \ref{tab:summary_thresh}, \ref{tab:adam_results_sim_type1}, \ref{tab:adam_results_sim_type2}, \ref{tab:adam_results_sim_type3}, \ref{tab:adam_results_sim_type4}
% , \ref{tab:adam_results_sim_type5} and \ref{tab:adam_results_sim_type6}.)
\begin{table}[htb]
\centering
 \begin{tabular}{l|l|c|c|}
\multicolumn{2}{c}{}&\multicolumn{2}{c}{Predicted}\\
\cline{3-4}
\multicolumn{2}{c|}{}&Positive&Negative\\
\cline{2-4}
\multirow{2}{*}{Actual class}& Positive & 21 & 0 \\
\cline{2-4}
& Negative & 37 & 0 \\
\cline{2-4}
\end{tabular}
\caption{ Confusion matrix for the results of the simulator on the Adam dataset for type 1}
\label{tab:adam_results_sim_type1}
\end{table} 

\begin{table}[htb]
\centering
 \begin{tabular}{l|l|c|c|}
\multicolumn{2}{c}{}&\multicolumn{2}{c}{Predicted}\\
\cline{3-4}
\multicolumn{2}{c|}{}&Positive&Negative\\
\cline{2-4}
\multirow{2}{*}{Actual class}& Positive & 40 & 0 \\
\cline{2-4}
& Negative & 18 & 0 \\
\cline{2-4}
\end{tabular}
\caption{ Confusion matrix for the results of the simulator on the Adam dataset for type 2}
\label{tab:adam_results_sim_type2}
\end{table}

\begin{table}[htb]
\centering
 \begin{tabular}{l|l|c|c|}
\multicolumn{2}{c}{}&\multicolumn{2}{c}{Predicted}\\
\cline{3-4}
\multicolumn{2}{c|}{}&Positive&Negative\\
\cline{2-4}
\multirow{2}{*}{Actual class}& Positive & 43 & 0 \\
\cline{2-4}
& Negative & 15 & 0 \\
\cline{2-4}
\end{tabular}
\caption{ Confusion matrix for the results of the simulator on the Adam dataset for type 3}
\label{tab:adam_results_sim_type3}
\end{table}


\begin{table}[htb]
\centering
 \begin{tabular}{l|l|c|c|}
\multicolumn{2}{c}{}&\multicolumn{2}{c}{Predicted}\\
\cline{3-4}
\multicolumn{2}{c|}{}&Positive&Negative\\
\cline{2-4}
\multirow{2}{*}{Actual class}& Positive & 47 & 0 \\
\cline{2-4}
& Negative & 11 & 0 \\
\cline{2-4}
\end{tabular}
\caption{ Confusion matrix for the results of the simulator on the Adam dataset for type 4}
\label{tab:adam_results_sim_type4}
\end{table}

\begin{table}[htb]
\centering
 \begin{tabular}{l|l|c|c|}
\multicolumn{2}{c}{}&\multicolumn{2}{c}{Predicted}\\
\cline{3-4}
\multicolumn{2}{c|}{}&Positive&Negative\\
\cline{2-4}
\multirow{2}{*}{Actual class}& Positive & 47 & 0 \\
\cline{2-4}
& Negative & 11 & 0 \\
\cline{2-4}
\end{tabular}
\caption{ Confusion matrix for the results of the simulator on the Adam dataset for type 5}
\label{tab:adam_results_sim_type5}
\end{table}

\begin{table}[htb]
\centering
 \begin{tabular}{l|l|c|c|}
\multicolumn{2}{c}{}&\multicolumn{2}{c}{Predicted}\\
\cline{3-4}
\multicolumn{2}{c|}{}&Positive&Negative\\
\cline{2-4}
\multirow{2}{*}{Actual class}& Positive & 21 & 0 \\
\cline{2-4}
& Negative & 37 & 0 \\
\cline{2-4}
\end{tabular}
\caption{ Confusion matrix for the results of the simulator on the Adam dataset for type 6}
\label{tab:adam_results_sim_type6}
\end{table}


\begin{table}[htb]
	\centering
   % \resizebox{0.3\textwidth}{!}
   % {
    \begin{tabular}{ccccc}
    \hline
    Type&Sensitivity& Specificity& Accuracy& F1-score\\ \hline
type 1 & 1.0 & 0.0 & 0.3621 & 0.5316\\ 
type 2 & 1.0 & 0.0 & 0.6897& 0.8163\\ 
type 3 & 1.0 & 0.0 & 0.7414& 0.8515\\ 
type 4 & 1.0 & 0.0 & 0.8103& 0.8952\\
type 5 & 1.0 & 0.0 & 0.9138& 0.9550\\
type 6 & 1.0 & 0.0 & 0.8103& 0.8952\\
    \hline
    \end{tabular}
  %  }
    \caption{ Summary of the results for different thresholds. }
	\label{tab:summary_type_results}

\end{table}
\subsubsection{Further steps}
Even though the results for this diauxic shift node are dissapointing, we still have some ideas on how the boolean network can still be used:
\begin{itemize}
 \item Use the boolean or continuous simulator combined with Dynamic Regulated Flux Balance Analysis to obtain growth curves.
 \item Evaluate attractor states of the network and see if they can be linked to phenotypes.
 \item Extract graph features for genes.
 \item Edges present in the model have a higher chance of being present in the ``real'' network.
\end{itemize}