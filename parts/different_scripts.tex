\chapter{Extensions to R}
\section{Introduction}
As the simulator for gene regulation is written in C++ and the simulator for the metabolic network (i.e. the Flux Balance Analysis) is written
in R, we made the decision to integrate all the code in R. Alternatives we considered were:
\begin{itemize}
 \item Integration in C++ with the library RInside\footnote{More info on http://dirk.eddelbuettel.com/code/rinside.html}: preliminary results showed that this would not be very efficient
 \item Using the executables and interface through files: this was expected to be inefficient.
\end{itemize}
We therefore opted to use the library RCpp\footnote{http://dirk.eddelbuettel.com/code/rcpp.html}\cite{rcpp} which allows the usage of C++ 
code (including classes) in R by linking to a C++ library. The part of the C++ codebase that can be used directly in R needs to be \textit{exposed}
by using some methods defined in the RCpp library. In section \ref{seq:exposed}, we will give an overview of the exposed classes and methods,
and in \ref{sec:example}, we will give an example of how to use the code in R.

\section{Classes and methods exposed from C++ to R}
In additition to the simulator exectuble that is generated when compiling the C++ code, part of the codebase is also exposed to R and can easily be linked with R code.
A full overview of the methods that are exposed can be found in the following 2 files in the code repository:
\begin{itemize}
 \item \url{AdaLab/src/R_interface.cpp}: This includes classes and methods related to the simulation of the gene regulatory network
 \item \url{AdaLab/src/R_interface_optimization.cpp}: This includes classes and methods for optimization algorithms
\end{itemize}
\section{Available R code related to simulation based optimization}
All R methods needed for simulation based optimization are located in the code repository at \url{AdaLab/MetabolicModel/R}

\section{Available R scripts}
Some scripts have already been developed 
In \url{AdaLab/MetabolicModel/R}

\begin{itemize}
 \item \url{find_grn.R}: Script to find the network and related parameters with Metropolis-Hastings methods.
 \item \url{test_all}: Script to run all tests found in \url{AdaLab/MetabolicModel/R/test}
 \item \url{options.R}: Script for all the options to run find_grn.R
 \item \url{growth_curves_for_interesting_TFs.R}: Script for generating the growth curves for the most interesting transcription factors provided
 by Daniel
 
\end{itemize}

%Notes about Rcpp -> save to file will not be able to reload c++ object!

\section{Other}
\begin{itemize}
 \item \url{help_functions.R}
 \item \url{simulation.R}
\end{itemize}
\section{Exposed classes and methods}\label{seq:exposed}
The currently exposed classes and methods can be found in the file \url{R_interface.cpp}. 
\begin{lstlisting}[language=C++,commentstyle=\mycommentstyle, caption={Exposed classes and methods}]

/* 
The List of classes available in R
Objects of these classes can be created in R with their respective contstructors
*/

RCPP_EXPOSED_CLASS(EveSimulatorBool)
RCPP_EXPOSED_CLASS(EveSimulatorDouble)
RCPP_EXPOSED_CLASS(Experiment)
RCPP_EXPOSED_CLASS(VertexDouble)
RCPP_EXPOSED_CLASS(VertexBool)
RCPP_EXPOSED_CLASS(Edge)
RCPP_EXPOSED_CLASS(ExperimentalOutput)


RCPP_MODULE(simulator){
    //Boolean simulator exposed methods
    Rcpp::class_<EveSimulator<bool>>("EveSimulatorBool")
/*
EveSimulator()
Creates an object of C++ type EveSimulator<bool>, equivalent to type EveSimulatorBool in R
@param: N.A.
@return: The EveSimulator object (with an empty network) 
*/
        .constructor()
        
        
        
  /*
EveSimulator(const std::string toy_network_file_name, const std::string noise_distribution, const double noise_param, const std::string & xml_nodes);
Creates an object of C++ type EveSimulator<bool>, equivalent to type EveSimulatorBool in R
@param: string network_file_name, path to the document that contains the network
	string noise_distribution; Distribution to add noise to the expression levels, "uniform" or "normal"
	double noise_param: for uniform noise this corresponds to the width, for gaussian noise to the standard deviation. 
			    Set this to 0 if no noise is required.
@return: The EveSimulator object (with an initialized network)
*/
        .constructor<std::string, std::string, double, std::string>() 
        
        
        
/*
void random_initialize_node_values(const float ratio= 0.5, const bool only_genes = true)
Initializes the initial value of the nodes randomly to true or false, with ratio of false equal to parameter ratio
@param: float ratio (optional): Relative amount of nodes that should get a value of false
        bool only_genes (optional): Only randomly initialize nodes associated with genes
@returns: void
*/
        .method("random_initialize_node_values", &EveSimulator<bool>::random_initialize_node_values)
        
        
        
/*
void initialize_node_values_with_map( const std::vector<std::string> & keys, const std::vector<double> &values)
Initializes the initial value of the nodes with the provided list of keys (node ids) and values(node values)
@precondition: The vector keys and values should have the same size, i.e. they are regarded as a one-to-one map
@param: vector<string> keys: A vector containing the id of the nodes
	vector<double> values: A vector containing the respective values for the nodes (i.e. keys[i] should get value values[i] for all i)
@returns: void
*/
        .method("initialize_node_values_with_map", &EveSimulator<bool>::initialize_node_values_with_map)
        
        
        
/*
std::vector<double> run_experiment_time_step(const Experiment & e, const std::vector<std::string> & metabolite_keys,
const std::vector<double> & metabolite_vals, const int iteration, const int timesteps, const std::vector<std::string> & gene_keys );
Runs an experiment and adjusts the values of the nodes for the next timesteps
@params: Experiment e: Object of type Experiment that describes the experiment
         vector<string> metabolite_keys: The ids of the metabolites for which the values have to be set
         vector<double> metabolite_values: The respective values of the metabolites in metabolite_keys (obtained by FBA)
         int iteration: Integer specifying which iteration has to be executed
         int timesteps: How many time steps this iteration has to do
         vector<string> gene_keys: The ids of the nodes for which the values have to be returned
@return: vector<double> gene_values: The values of the nodes associated with gene_keys

*/

        .method("run_experiment_time_step", &EveSimulator<bool>::run_experiment_time_step)
        
        
        
/*
void reset_state_map()
Resets the values of the nodes in the network to the initial state.
@param: Nothing
@returns: void
*/
        .method("reset_state_map", &EveSimulator<bool>::reset_state_map)
        
        
        
        ;
    Rcpp::class_<EveSimulator<double>>("EveSimulatorDouble")
        /*
        EveSimulator()
	Creates an object of C++ type EveSimulator<double>, equivalent to type EveSimulatorDouble in R
	@param: N.A.
	@return: The EveSimulator object (with an empty network) 
        */
        .constructor()
        
        
        
	/*
        EveSimulator(const std::string toy_network_file_name, const std::string noise_distribution, const double noise_param, const std::string & xml_nodes);
	Creates an object of C++ type EveSimulator<double>, equivalent to type EveSimulatorDouble in R
	@param: string network_file_name, path to the document that contains the network
	        string noise_distribution; Distribution to add noise to the expression levels, "uniform" or "normal"
	        double noise_param: for uniform noise this corresponds to the width, for gaussian noise to the standard deviation. Set this to 0 if no noise is required.
	@return: The EveSimulator object (with an initialized network)
        */
        .constructor<std::string, std::string, double, std::string>()
        
        
        
         /*
	void random_initialize_node_values(const float ratio= 0.5, const bool only_genes = true)
	Initializes the initial value of the nodes randomly to -1. or +1., with ratio of -1.'s equal to parameter ratio
	@param: float ratio (optional): Relative amount of nodes that should get a value of -1.
	        bool only_genes (optional): Only randomly initialize nodes associated with genes
	@returns: void
        */
        .method("random_initialize_node_values", &EveSimulator<double>::random_initialize_node_values)
        
        
        
	/*
	void initialize_node_values_with_map( const std::vector<std::string> & keys, const std::vector<double> &values)
	Initializes the initial value of the nodes with the provided list of keys (node ids) and values(node values)
	@precondition: The vector keys and values should have the same size, i.e. they are regarded as a one-to-one map
	@param: vector<string> keys: A vector containing the id of the nodes
	        vector<double> values: A vector containing the respective values for the nodes (i.e. keys[i] should get value values[i] for all i)
	@returns: void
        */
        .method("initialize_node_values_with_map", &EveSimulator<double>::initialize_node_values_with_map)
        
        
        
        /*
std::vector<double> run_experiment_time_step(const Experiment & e, const std::vector<std::string> & metabolite_keys,
const std::vector<double> & metabolite_vals, const int iteration, const int timesteps, const std::vector<std::string> & gene_keys );
Runs an experiment and adjusts the values of the nodes for the next timesteps
@params: Experiment e: Object of type Experiment that describes the experiment
         vector<string> metabolite_keys: The ids of the metabolites for which the values have to be set
         vector<double> metabolite_values: The respective values of the metabolites in metabolite_keys (obtained by FBA)
         int iteration: Integer specifying which iteration has to be executed
         int timesteps: How many time steps this iteration has to do
         vector<string> gene_keys: The ids of the nodes for which the values have to be returned
@return: vector<double> gene_values: The values of the nodes associated with gene_keys

*/
        .method("run_experiment_time_step", &EveSimulator<double>::run_experiment_time_step)
        
        
        
/*
void reset_state_map()
Resets the values of the nodes in the network to the initial state.
@param: Nothing
@returns: void
*/
	.method("reset_state_map", &EveSimulator<double>::reset_state_map)
	
	
	

            ;
    Rcpp::class_<Experiment>("Experiment")
/*
Experiment( const std::string experiment_file, const std::string xml_nodes = std::string("") )
Creates an object of Experiment based on the description in the file pointed to by experiment_file.
@params: string experiment_file: Path to file that contains description of experiment
	 string xml_nodes(optional) : Path to file that contains types of node ids [legacy]
@returns: Object of class Experiment
*/
    .constructor<std::string,std::string>()
    
    
    
/*
Experiment(  )
Creates an object of Experiment (default empty).
@params: Nothing
@returns: Object of class Experiment
*/
    .constructor()

}
\end{lstlisting}

\section{Example in R}
Prior to running the example in R, make sure to compile the c++ codebase first\footnote{This is described in section \label{seq:installation}}. If no errors occur, the newly created library should be available
in the MetabolicModel directory of the project code repository. On Linux, this file this executable is called \url{libadalab.so}.
\begin{lstlisting}[language=R,commentstyle=\mycommentstyle, caption={Example in R of how to use the gene regulation simulator}]
#Load libraries that allow to use c++ code
library(Rcpp)
library(inline)
#Load the adalab c++ code base
libadalab <- dyn.load("libadalab.so")

#Some general parameters, be sure that the paths are changed/corrected
root <- "/home/alexandros/"
path_cont_network <-  paste(root, "AdaLab/Data/regression_param_report30minSVRLin.xml", sep = '', collapse = '')
path_xml_nodes <- paste(root,"AdaLab/Data/nodetypes.xml", sep = '', collapse = '')
experiment_file <- paste(root,"AdaLab/Data/experiment.xml", sep = '', collapse = '')
path_cont_test_network <- paste( root, "AdaLab/src/network/test/test_cont_network.xml", sep = '', collapse = '')

#Load the module "simulator", this contains the exposed classes and methods
loadModule("simulator", TRUE)
simulator <- Module( "simulator", PACKAGE=libadalab )

#Get the constructors from the module: EveSimulatorDouble will construct a continuous simulator
EveSimulatorDouble <- simulator$EveSimulatorDouble
#Get the constructor for the experiment
Experiment <- simulator$Experiment

#Now create simulator objects:
#The continuous one:
eve_double <- new(EveSimulatorDouble, path_cont_network , "uniform", 0., "") #constructor

#Now create an experiment defined in some file:
print( "-----------------------------------Creating experiment---------------------------")
exp <- new( Experiment, experiment_file )
print( "-----------------------------------End experiment---------------------------")

###TESTING, this uses the simpler test networks
gene_keys <- c("A",  "C") #keys(names in network) of the genes we need to feed back to the metabolic network
print("-------------------------------------Boolean test---------------------------------")

#cfr. EveSimulator constructor: parameters: network_file, noise distribution, noise_parameter, xml_nodes
eve_test_bool <- new(EveSimulatorBool, path_bool_test_network, "uniform", 0., "")

eve_test_experiment <- new(Experiment)
initial_keys <- c("A","B", "C")  # Define a list of nodes for which the initial nodes have to be set ( by default all non-experimental nodes are set to 0)
initial_values <- c(0.,0.,0.) # List with the actual initial values of the nodes
# Initialize the network with specified lists
print("-----------------------------------Initializing node_values--------------------------")
eve_test_bool$initialize_node_values_with_map(initial_keys, initial_values)
print("end initialization")

timesteps_per_iteration_cont <- 1 #
print("-------------------------------------Continuous test---------------------------------")
eve_test_cont <- new(EveSimulatorDouble, path_cont_test_network ,"uniform", 0., "")
initial_keys <- c("A","B","C","D")
initial_values <- c(0.,0.,0.,0.)
gene_keys <- c("A", "B", "C")

eve_test_cont$initialize_node_values_with_map(initial_keys, initial_values)
for(time in 0:50)
{
    metabolite_keys <- c("D")
    metabolite_values <- c(0.1)
    iteration <- time
    gene_values <- eve_test_cont$run_experiment_time_step(eve_test_experiment, 
							  metabolite_keys, 
							  metabolite_values, 
							  iteration, 
							  timesteps_per_iteration_cont,
							  gene_keys)
}

  eve_test_cont$reset_state_map()


\end{lstlisting}

