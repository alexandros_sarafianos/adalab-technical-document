\section{ Continuous network }\label{sec:continuous_network}
\subsection{Introduction}
We want to learn a function $f$ that maps an experiment $\mathbf{e}$ to a time series of node states. More formally: $ f: \mathbf{e} \to \mathbb{R}^{N \times T}$ where $N$ is the amount of nodes in the network and $T$ the timepoints for which the values 
have to be estimated and $\mathbf{e}$ is a tuple corresponding to a biological experiment. To this purpose we learn a function $g_i$ for each node $i$ in the network that estimates the value of the next time step given the values of the current time.
\subsection{Datasets}
Available datasets can typically be divided in two types: time-course experiments to observe changes in expression levels over time and perturbation experiments to observe effects of a change or treatment on a cell. Since our interest with 
regard to the gene regulatory network lies on the temporal behaviour of gene regulation i.e. the dynamical behaviour, we only selected timeseries datasets from SGD \footnote{http://www.yeastgenome.org/download-data/expression}. We did not 
limit ourselves to datasets solely concerning diauxic shift as we assume that the genetic influences can be averaged over the different conditions. 
This resulted in the following subset: \cite{brauer2005homeostatic}, \cite{spellman1998comprehensive}, \cite{cho1998genome}, \cite{derisi1997exploring},
\cite{lee2000arrest} and \cite{carmel2001role}.
Prior to regression tasks, the datasets were normalized (independently from eachother) such that all expression values are contained in the interval $[-1,1]$.%http://libsleipnir.bitbucket.org/Normalizer.html
\subsection{ Processing the Zimmer model }
As shown in section \ref{sec:zimmer}, the nodes in the Zimmer model are not only genes, but also metabolites, complexes, drugs and conditions. In contrast to this, typically gene
expression timeseries datasets do not contain information about the concentration of metabolites throughout the experiment and as such no information is available for predictive purposes.
We therefore do not estimate the effect of non-gene nodes. Additionally we group all genes that have an effect on a particular gene together instead of dividing them
into different reactions. 
\subsection{Regression task}
\subsubsection{Introduction}
For the regression task of estimating the functions $g_i$ we consider two different models:
\begin{itemize}
 \item We assume that the level of a gene at a certain time is dependent on a linear combination of the genes indicated to influence that gene and itself(indicated by Zimmer model) at the previous time. We also discretize time for this purpose and assume
gene level updates are done in a synchronous manner.
A gene level can therefore be obtained by the following formula:
\begin{equation}
 g_i(t+1) = \sum_{j : g_j \in Pa(g_i) \cup \{g_i\} } w_{ij}g_j(t) + \beta_{i}
 \label{eq:cont_sim}
\end{equation}
where $g_k$ are genes, $t$ is time, $Pa(\dot)$ indicates the parents of a gene, $w_{ij}$ are weights and $\beta_i$ is an intercept. Mind that we also added $g_i(t)$ as we expect the current gene level to be dependent on the previous gene level. This
is also to ensure that genes that have no input can still change. 



\item Alternatively instead of predicting the actual gene expression level it is also possible to model the differential rates, i.e.:
\begin{equation}
 \frac{dg_i(t)}{dt} = f_i(g_{i1}(t), g_{i2}(t),...)
\end{equation}
If these time intervals $dt$ are small, this can be approximated by:
\begin{equation}
 \frac{g_i(t_j+1) - g_i(t_j)}{t_{j+1}-t_j} \approx f_i(g_{i1}(t), g_{i2}(t),...)
\end{equation}
With $t_j$ and $t_{j+1}$ two consecutive (discrete) time points.
For $f_i(g_{i1}(t), g_{i2}(t),...)$ we take the following equation:
\begin{equation}
 f_i(g_{i1}(t), g_{i2}(t),...) = w_{i1}g_1(t)+ ... w_{in}g_n(t) + \beta_i
\end{equation}
These models are called linearized additive models \cite{de2002modeling} and is equivalent to the first formula where $w_{ij} = 1$ for 
$g_i = g_j$. In the first case however the value of $w_{ij}$ is also being estimated.

\item Another possibility is to model gene regulation via a muliplicative model \cite{}, but this approach has not been attempted yet.
Using scikit-learn\footnote{scikit-learn.org} (Python) we obtain the necessary parameters, $w_ij$ and $\beta{i}$. 
\end{itemize}

\subsection{Results}%Script AdaLab/general_scripts/python/network_influences/main.py ``2''
\subsubsection{Data}
We obtain appropriate datasets by choosing a $t_{defined}$ and only selecting those datapoints for which we have data of $\Delta t = t_{defined}$. As the datasets are very diverse in their timescales, this results less available data per reaction. These time 
difference are expected to have a significant effect on the output. This resulted in datasets for which the summary can be found in table \ref{tab:summary_time_datasets}.
\begin{table}[htb]
	\centering
   % \resizebox{0.3\textwidth}{!}
   % {
    \begin{tabular}{llll}
    \hline
$\Delta t$(min)&Min&Max&Mean\\
\hline
10&37&94&82.14\\
15&58&111&101.91\\
20&7&53&44.42\\
30&86&170&153.04\\
60&70&136&121.32\\
120&22&52&46.68\\
180&11&31&27.95\\
    \hline
    \end{tabular}
  %  }
    \caption{ Summary of amount of datapoints available per reaction for different time differences. Reported are the minimum amount of data for a reaction, the maximum amount and the average amount. }
	\label{tab:summary_time_datasets}

\end{table}
\subsubsection{Version 1}
The results for the first version are summarized in table \ref{tab:summary_v_1}, which shows different qualitative measures for the different regressors and for the different time discretizations.
Apart from a least-squares estimator, we also trained using grid search and 10-fold crossvalidation, a linear support vector 
regressor\footnote{with values $C \in \{ 2^k \forall k \in [-10,15[ : k \in \naturalspace \}$ and 
$\epsilon = \{0,0.01,0.05,0.1,0.15,0.2\}$ and L1 loss function},
a ridge regressor\footnote{$\alpha = \{0.1,1.0,10.0\}$}, 
a decision tree regressor\footnote{$\text{max\_depth} \forall i \in [2,50[: i \in \naturalspace$, 
$\text{max\_features} \in \{1,3,10\}$, 
$\text{min\_samples\_split} \in \{1,3,10\}$,
$\text{min\_samples\_leaf} \in \{1,3,10\}$,
$\text{bootstrap} \in \{True,False\}$,
criterions: entropy and gini.}.
After the hyperparameters were determined by crossvalidation, the regressor was retrained with the whole training set. Reported results are on a separate testset, divided in two 80\%-20\$ disjoint sets (respectively training and test set).
\input{parts/cont_version1_results}
\subsubsection{Version 2}
The results for the second version are summarized in table \ref{tab:summary_v_2}, which again shows different qualitative measures for the different regressors and for the different time discretizations.
The same regressors and hyperparameter tuning methodology as in the first version were used.
\input{parts/cont_version2_results}
\subsubsection{Extension}
Estimation the confidence interval for prediction errors of support vector machine classifiers\cite{jiang2008estimating}.
\subsubsection{Discussion}
For version 1 we see that for most different regressors either the 20 minutes or the 30 minutes version has best performance ( not taking into account statistical significance of differences ). Furthermore If we compare the best results of
every regressor, we see that the best one is a linear support vector regressor even though we did not take into ``or'' relations cfr. decision trees. Also The linear version is better than the RBF SVR, suggesting that a linear approximation is not
necessarily a very bad approximation. Even though we report some good results (around 0.08 for mae, which is 4\%), the influence of metabolic levels is not estimated and if we have access to metabolic levels, it is to be expected that performance would
improve. For version 2 however, the results are overall very poor.
\section{Estimating growth curves}
\subsection{Introduction}
$ f: \mathbf{x} \to \mathbb{R}^{T}$, function mapping $\mathbf{x}$, a tuple which corresponds to a biological experiment, to a time series that corresponds to a (discretized) growth curve.
The simulator is the central tool in the proposed system, it takes as an input: 
\begin{itemize}
 \item A modelspace $\modelspace$. The different modelspaces are discussed in section% \ref{sec:modelspaces}
 \item A probability space $\probmodelspace$ over the $\modelspace$.
 \item An experimentspace $\expspace$.
 \item A set of additional configurations
\end{itemize}
As output it generates:
\begin{itemize}
 \item A time series of gene expression levels
 \item A time series of metabolite levels
 \item A growth curve
\end{itemize}
The simulator can be used to estimate the usefullness of experiments given a distribution over networks, to estimate sensitivity of the inference to certain variations on the parameters of our models. 

\begin{figure}	
    \centering
    \includegraphics[width=0.4\textwidth]{Images/simulator.png}
    \caption{ Simulator }
    \label{fig:weighting}
\end{figure}


