\chapter{Integration with Flux Balance Analysis in R}
\section{Introduction}
As the simulator for gene regulation is written in C++ and the simulator for the metabolic network (i.e. the Flux Balance Analysis) is written
in R, we made the decision to integrate all the code in R. Alternatives we considered were:
\begin{itemize}
 \item Integration in C++ with the library RInside\footnote{More info on http://dirk.eddelbuettel.com/code/rinside.html}: preliminary results showed that this would not be very efficient
 \item Using the executables and interface through files: this was expected to be inefficient.
\end{itemize}
We therefore opted to use the library Rcpp\cite{rcpp} which allows the usage of C++ 
code (including classes) in R by linking to a C++ library. The part of the C++ codebase that can be used directly in R needs to be \textit{exposed}
by using some methods defined in the Rcpp library. In section \ref{seq:exposed}, we will give an overview of the exposed classes and methods,
and in \ref{seq:example_r}, we will give an example of how to use the code in R.
\section{Exposed classes and methods}\label{seq:exposed}
The currently exposed classes and methods can be found in the file \url{R_interface.cpp}. 
\begin{lstlisting}[language=C++,commentstyle=\mycommentstyle, caption={Exposed classes and methods}]

RCPP_MODULE(simulator){
    /** @brief Class to simulate gene regulatory networks. This will only allow deterministic parameters
        for the parameters of the network. General noise levels can still be added by setting the random_number_generator*/
    Rcpp::class_<EveSimulator<adalab::real>>("EveSimulatorDouble")
        /**
         * @brief Default constructorCreates a basic object with standard values. By default the underlying network will be empty.
         * @return the object
         * */
        .constructor()
        .constructor<std::string, std::string, adalab::real, std::string>()
        .constructor<std::string, std::string, adalab::real>()

        /** @brief Sets the required gene_keys. I.e. to indicate for which vertices the final values should be returned when
         * running run_experiment
         *  @param rh_gene_keys: Vector of keys (cfr. names of vertices)*/
        .method("set_required_gene_keys", &EveSimulator<adalab::real>::set_required_gene_keys)

        .method("initialize_node_values_with_map", &EveSimulator<adalab::real>::initialize_node_values_with_map)
        .method("initialize_node_values_with_map_str", &EveSimulator<adalab::real>::initialize_node_values_with_map_str)
        /** @brief Runs experiment and returns the final values for the nodes with ids in needed_gene_keys
         * @param An experiment e (object of class Experiment)
         * @param needed_gene_keys: a vector of strings corresponding to ids from vertices in the underlying network for which the final values should be returned.
         * (i.e. for instance to feed to the metabolic part)
         * @param metabolite_keys: A list of metabolites corresponding to the keys for metabolite_vals, the values associated with them should be adapted
         * @param metabolite_vals: The metabolite values corresponding to the metabolite keys (This is pulled apart as R does not allow to pass maps)
         * @returns: A vector containing the final values of the vertices as specified in needed_gene_keys(and in that order)
        */
        .method("run_experiment", &EveSimulator<adalab::real>::run_experiment )
        .method("print_adjacency_matrix_dimensions", &EveSimulator<adalab::real>::print_adjacency_matrix_dimensions)
        .method("get_adjacency_matrix_dimensions", &EveSimulator<adalab::real>::get_adjacency_matrix_dimensions)
        /** @brief Returns the adjacency matrix related to the underlying network in an R readable format
         *  @return An Rcpp::NumericMatrix containing the values of the parameters of the underlying network
        */
        .method("get_rcpp_adj_mat", &EveSimulator<adalab::real>::get_rcpp_adj_mat)
        /** @brief Returns the names of the vertices of the underlying network
         *  @return The names of the vertices, the order is related to the order in which the
         * vertices were added and is essential for interpreting the underlying adjacency matrix correctly*/
        .method("get_node_names", &EveSimulator<adalab::real>::get_node_names)
        /** @brief Returns nodes in the network and their type
         *  @return Returns a vector of pairs, in which the first element is the name of the vertex and the second element
         * the type (gene, metabolite, ...)
         * */
        .method("get_nodes", &EveSimulator<adalab::real>::get_nodes)

        /** @brief Returns information about degrees of the vertices of the underlying network
         *  @param node_names: A vector of the names of the vertices for which to get the edge information
         *  @return A vector for which every element is a vector containing the name of the vertex, the in_degree and
         * the out_degree (in that order)
         * */
        .method("get_edge_information", &EveSimulator<adalab::real>::get_edge_information)
        /** @brief Method creating a random neighbour (i.e. network with different edges) of the current network
         * The underlying Network object grn is adapted by choosing two vertices and removing the edge if it is present, or adding the edge if it is absent
         * */
        .method("random_neighbour", &EveSimulator<adalab::real>::random_neighbour )

        /** @brief Returns the boolean adjacency matrix of the underlying network in an R readable format
         *  @return An Rcpp::NumericMatrix of the adjacency matrix. 1 if there is an edge, 0 if there is no edge.
         *  columns/rows should be interpreted with get_node_names()
         * */
        .method("get_boolean_adjacency_matrix", &EveSimulator<adalab::real>::get_boolean_adjacency_matrix)


        /** These functions where added in order define a copy operation in R for EveSimulator C++ objects.
         *  Related to the issue described in
         * http://stackoverflow.com/questions/33803171/copy-underlying-c-object-in-r-when-exposing-the-class-with-rcpp-modules
         * ==================================================================================================================*/
        .method("get_random_number_generator_string", &EveSimulator<adalab::real>::get_random_number_generator_string)
        .method("get_times", &EveSimulator<adalab::real>::get_times)
        .method("get_required_gene_keys", &EveSimulator<adalab::real>::get_required_gene_keys)
        .method("get_metabolite_keys", &EveSimulator<adalab::real>::get_metabolite_keys)
        .method("get_node_names", &EveSimulator<adalab::real>::get_node_names)
        .method("get_nodes_to_be_extracted", &EveSimulator<adalab::real>::get_nodes_to_be_extracted)
        .method("get_time_series_size", &EveSimulator<adalab::real>::get_time_series_size)
        .method("get_only_update_specific_subset", &EveSimulator<adalab::real>::get_only_update_specific_subset)
        .method("get_network", &EveSimulator<adalab::real>::get_network_d)
        .method("get_discrete_t", &EveSimulator<adalab::real>::get_discrete_t )
        .method("set_random_number_generator_string", &EveSimulator<adalab::real>::set_random_number_generator_string)
        .method("set_times", &EveSimulator<adalab::real>::set_times)
        .method("set_required_gene_keys", &EveSimulator<adalab::real>::set_required_gene_keys)
        .method("set_metabolite_keys", &EveSimulator<adalab::real>::set_metabolite_keys)
        .method("set_node_names", &EveSimulator<adalab::real>::set_node_names)
        .method("set_nodes_to_be_extracted", &EveSimulator<adalab::real>::set_nodes_to_be_extracted)
        .method("set_time_series_size", &EveSimulator<adalab::real>::set_time_series_size)
        .method("set_only_update_specific_subset", &EveSimulator<adalab::real>::set_only_update_specific_subset)
        /** @brief Initializes the network for the simulator.
         *  @pre node_names should already be set
         *  @param params: a vector containing the params of the vector. Should either be the size of the amount
         * of 1's in adj_mat or the size of adj_mat
         *  @param adj_mat: R matrix to indicate the structure. If adj_mat[i][j] == 1, there is
         * an edge between node_names[i] and node_names[j]
         * */
        .method("initialize_network", &EveSimulator<adalab::real>::initialize_network<adalab::real>)
        /*==================================================================================================================*/


        /** @brief Sets the structure of the network to allow for easier updates of the parameters of the networks during optimization
         *         When updating the parameters, only the values corresponding to the structure should be given.
        */
        .method("set_fixed_structure_for_optimization", &EveSimulator<adalab::real>::set_fixed_structure_for_optimization)
        /** @brief Resets the state_map variable of all vertices in the network, this includes:
         *    - resizing it to size1
         *    - initializing the first value to a default (0)
         * Additionally the discrete_t variable will be reset to 0
        */
        .method("reset_state_map", &EveSimulator<adalab::real>::reset_state_map)
            ;

    /** @brief Class to simulate gene regulatory networks. This will only allow deterministic parameters
        for the parameters of the network. General noise levels can still be added by setting the random_number_generator*/

    // The methods and how to use them are the same as in EveSimulatorProb
    // Omitting the methods for the deterministic case
     /** @brief class that can store experimental information about a single biological entity (without storing the name of the biological entity)*/
    Rcpp::class_<ExperimentalInfo>("ExperimentalInfo")
            /** @brief Default constructor*/
            .constructor()
            /** @brief Basic constructor
             *  @param m_value: the value(double) some biological entity should have
             *  @param fixed: boolean value specifying whether this value should be kept fixed when generating time series of the values of the vertices
             *  @return object of ExperimentalInfo*/
            .constructor<adalab::real, bool>()
            /** @brief Basic constructor for specifying experimental condition as a probability distribution
             *  @param m_value: string containing the distribution of the experimental condition.
             * This should be of the form of "<distr>(<val1>, <val2>)". Where <distr> is equal to:
             *  - "N" or "normal" for the normal distribution
             *  - "U" or "uniform"  for the uniform distribution
             *  - "U_int" or "uniform_int" for the integer uniform distribution
             *  - "beta" for the beta distribution
             * <val1> and <val2> should be numerical values (i.e. integers or doubles)
             * @return object of ExperimentalInfo */
            ;
    /** @brief Experiment is a class that bundles experimental conditions to run*/
    Rcpp::class_<Experiment>("Experiment")
    //!  @brief Default constructor
    .constructor()
    /** @brief Add an additional condition to the experiment
     * @param:   key: String with the name of gene/compound/metabolite... for which to add the condition defined by info. Should be present in the network
     *                in order to have an effect on the network.
     *                In case another condition was already present, it is overwritten
     * @param:   info: the experimental contition to add for key
     * @return:  void
     * */
    .method("add_condition", &Experiment::add_condition)
    /** @brief Add a knocked out gene to the experiment.
     * @param   gene_name: String with the name of the gene. Ideally should correspond to the name of a vertex in the network.
                            In case another condition was already present, it is overwritten
     * @param
     * @return  void
     * */
    .method("add_knockout_gene", &Experiment::add_knockout_gene)
    /** @brief Remove conditions from an experiment
     * @param:   key: String with the name of gene/compound/metabolite... for which to remove the conditions
     *                In case no conditions were present for that key, nothing will be removed
     * @return:  void
     * */
    .method("remove_condition", &Experiment::remove_condition)
    /** @brief Returns a boolean value to see if the experiment contains any experimental condition for a given key
     * @param:   key: String with the name of gene/compound/metabolite... for which to check for conditions
     * @return:  boolean, true if there is a condition present for key, otherwise false
     * */
    .method("does_condition_exist", &Experiment::does_condition_exist)
    .method("print_experiment", &Experiment::print_experiment)
         ;
    /** @brief Class to enable same api for different probability distributions (for instance from boost) */
    Rcpp::class_<RandomWrapper>("RandomWrapper")
            //! @brief Default Constructor, when using this to generate random values, only 0 will be returned
            .constructor()
            /** @brief Constructor taking a string that symbolizes a probability distribution
             *  @param distribution_string: string symbolizing a probability distribution or "noisefree". In case
             *  of a distribution should be of the form of "<distr>(<val1>, <val2>)". Where <distr> is equal to:
             *  - "N" or "normal" for the normal distribution
             *  - "U" or "uniform"  for the uniform distribution
             *  - "U_int" or "uniform_int" for the integer uniform distribution
             *  - "beta" for the beta distribution
             * <val1> and <val2> should be numerical values (i.e. integers or doubles)
             * **/
            .constructor<std::string>()
            /** @brief Generates a random value based on the initialized distribution
             *  @return The random value
             * */
            .method("get_random_value", RandomWrapper::operator() )
            ;

    /** @brief Class that stores a network structure to be used as a prior, it can also calculate the prior probability*/
    Rcpp::class_<SimulatorPrior<EveSimulator<adalab::real>>>("RSimulatorPrior")
            /** @brief Default constructor */
            .constructor()         
            /** @brief Constructor from file
             *  @param zimmer_file: string of the path to the network file(format described as in simulator doc)
             *  @param zimmer_edge_prior:  prior probability of edge being present in real network and prior
             *  @param zimmer_absent_edge_prior:    prior probability of edge being absent in both real network and prior
             * */
            .constructor<std::string,double,double >()
            /** @brief Calculates and returns the prior probability of a certain network structure
             *  @param node_names: vector of names of the vertices in the network in order to correctly interpret the adjacency matrix
             *  @param mat: boolean adjacency matrix, contains a one at a location mat[i][j] if there is an edge
             * between node_names[i] and node_names[j]
             *  @return The prior probability for the network structure
             */
            .method("get_prior", &SimulatorPrior<EveSimulator<adalab::real>>::get_prior)
            ;
\end{lstlisting}
Prior to running the example in R, make sure to compile the c++ codebase first\footnote{This is described in section \label{seq:installation}}. If no errors occur, the newly created library should be available
in the MetabolicModel directory of the project code repository. On Linux, this file this executable is called \url{libadalab.so}. Additionally, to test all R functionality, run the script test\_all.R in 
\url{AdaLab/MetabolicModel/R}.
\section{Example in R}\label{seq:example_r}

\begin{lstlisting}[language=R,commentstyle=\mycommentstyle, caption={Example in R}]
library("optparse")

# Link to the correct locations of the files when runnning this code!
option_list = list(
  make_option(c("-l", "--libadalab"), type="character", default="/home/alexs/AdaLab/MetabolicModel/libadalab.so", help="full path to libadalab.so", metavar="character", dest = "libadalab_location"),

  make_option(c("-n", "--network"), type="character", default="/home/alexs/AdaLab/Data/regression_param_report30minSVRLin.xml", help="Full path initial network file", metavar="character", dest = "path_cont_network"),

  make_option(c("-pr", "--prob"), type = "logical", default = FALSE, action = "store_true"),
  
  make_option(c("-gl", "--genelevels"), type = "character", default ="/home/alexs/AdaLab/Data/initial_gene_levels/brauer_initial_1.txt", metavar = "character" )

);
opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);

#Load the C++ shared library and module simulator
libadalab <- dyn.load(libadalab_location)
loadModule("simulator", TRUE)
simulator <- Module( "simulator", PACKAGE=libadalab )
#Make a simulator object, for a probabilistic one replace EveSimulatorDouble with EveSimulatorProb
sim <- new(simulator$EveSimulatorDouble, path_cont_network , "uniform", 0., "")

#Print the names of the vertices
print(sim$get_node_names())

#Create an experiment
exp <- new( simulator$Experiment )
exp$add_knockout_gene("YPL089C")

#Run an experiment
gene_values <- object$eve_double$run_experiment( exp, #the experiment,
					         c("YGL253W", "YML007W"), #Keys to extract
						 c("nitrogen"), #list with metabolite names to update
						 c(0.2) #Value for nitrogen
						  )
\end{lstlisting}
