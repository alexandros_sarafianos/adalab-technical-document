\subsection{Format of input files}
\subsubsection{Node file}\label{sec:file_format_xml}
XML file extracted from the exported xml files from \url{https://services.bio.ifi.lmu.de/diauxicGRN/}. This provides information about the types of the nodes, i.e. are they metabolites, genes, etc.
An example can be found in \url{<projectdirectory>/Data/nodetypes.xml}.
\subsubsection{Boolean Network file}\label{sec:file_format_network}
Example extract of the boolean network file:
\begin{lstlisting}
^nitrogen -> DCS2
^cAMP&nitrogen -| DCS2
sorbac -> TDH1
GCR1&glucose -> TDH1
\end{lstlisting}
\begin{itemize}
 \item \string^: boolean not
 \item \string&: boolean and
 \item $\rightarrow$: activation
 \item $\dashv$:  inhibition
 \item Variables nitrogen, DCS2,... as defined in section \ref{sec:file_format_xml}
 \item Multiple reactions on the same node are allowed and handled as logical or statements (e.g. DCS2 in the example)
 \item Lines starting with \# are ignored
 \end{itemize}
\subsubsection{Continuous network file}
An extract of a continuous network file:
\begin{lstlisting}
<network>
<reaction>
<?size:149?>
<input symbol="YOR363C" weight="0.239363154691"/>
<input symbol="YBR159W" weight="0.613708327904"/>
<input symbol="YAL051W" weight="0.0246455761393"/>
<input symbol="oleic_acid" weight="NA" type="met"/>
<input symbol="glucose" weight="NA" type="met"/>
<constant weight="-0.00649841561069"/>
<output symbol="YBR159W"/>
</reaction>
</network>
\end{lstlisting}
This symbolizes the reaction $ 0.239363154691*YOR363C[t] + 0.613708327904*YBR159W[t] + 0.0246455761393*YAL051W[t]
-0.00649841561069 =  YBR159W[t+1]$. When the weight is ``NA'' it is initialized to 0.
Additionally there is an extended continuous network file format which is slightly different. 
\begin{lstlisting}
<network>
<reaction>
<input symbol="YDR477W"/>
<input symbol="YGL179C" />
<output symbol ="YPR199C"/>
<function>
<functiontype type="linear"/>
<parameter symbol="YDR477W" pdf="N(0.00405665,1.)" />
<parameter symbol="YGL179C" pdf="N(0.00405665,1.)" />
<constant pdf="N(0.2,0.5)"/>
</function>
</reaction>
</network>
\end{lstlisting}
In this case, structure and function is separated. All variables are seen as random variables and the functiontype specifies
how to combine them (in this case a linear combination). More elaborate examples of these files are available at respectively
\url{AdaLab/Data/regression_param_report30minSVRLin.xml} and at \url{AdaLab/src/io/test/prob_network.xml}.
\subsubsection{Experiment file}\label{sec:file_format_experiment}
%{Example that can be found in \url{\<projectdirectory\>/Data/experiment.xml}
Example that can be found in \url{<projectdirectory>/Data/experiment.xml}:
\begin{lstlisting}[language=XML,basicstyle=\ttfamily,breaklines=true]
<experiment>
<genes>
<gene id="ADR1" value="0" fixed ="1"/>
</genes>
<metabolites>
<metabolite id= "glucoseext" value="1" fixed="0"/>
</metabolites>
<drug></drug>
<condition></condition>
</experiment>
\end{lstlisting}
Every experimental factor has to have three attributes: id, value and fixed.
The id specifies which node of the network needs to be adjusted. For the boolean case, value should be $\in \{0,1\}$. 0 meaning inactive, 1 meaning active. By default unspecified nodes are initialized to 0. Finally the fixed 
attribute signifies whether the value of a certain node is allowed to change based on the boolean rules or has to remain fixed (e.g. for a knocked out gene we want the value to remain 0).
A check for valid ids is done, the value and fixed parameters are currently not verified. The list of possible ids is generated from the descriptor-nodes file, which is by default \url{<workdirectory>/Data/nodetypes.xml}. 
\subsection{Command-line parameters}
As mentioned in section \ref{subsubsec:build}, the executable of the simulator can be found in \url{AdaLab/build/src/simulator}. Simply running the simulator
will not be sufficient, as some input parameters have to be specified. An overview of these are shown in table \ref{tab:simulator_input}.

\begin{sidewaystable}[htb]
	\centering
\resizebox{0.7\textwidth}{!}
    {
    \begin{tabular}{lllll}
    \hline
    Parameter&Symbol&Description&Possible values&Default value\\ \hline
    projectdirectory&p&Path to the project directory AdaLab&Any valid path&\url{<environment variable ``HOME''>/AdaLab} \\
    descriptor-nodes&d&Path to file with nodes contained in network file&Paths to any files that satisfy the format specified in section \ref{sec:file_format_xml}&\url{<workdirectory>/Data/nodetypes.xml} \\
    network-file&n&Path to file specifying the boolean relations between nodes& Paths to any files that satisfy the format specified in section \ref{sec:file_format_network}&\url{<workdirectory>/Data/zimmer_boolean_network.txt} \\
    experiment-file&e&Path to file with experiment to perform&Paths to any files that satisfy the format specified in section \ref{sec:file_format_experiment}&\url{<workdirectory>/Data/experiment.xml}    \\
    time-steps&t&Amount of times values have to be propagated through the network&Integer values greater than 0&50\\
    save-output&o&Saves additional output in \url{<workdirectory>/.simulator_experiments}&True: saves output, false: Don't save&False \\
    silent&s&Parameter controlling logging&true:no logging, false:logging. Logs are kept in \url{<workdirectory>/.simulator_experiments} &false\\
    config-file&c&Path to config file&Paths to any valid config file&No default value\\
    \hline
    \end{tabular}
    }
    \caption{ List of possible inputs for the simulator }
	\label{tab:simulator_input}
	\end{sidewaystable}

To run the simulator if you are in the directory containing the executable and the descriptor-nodes, network-file and experiment file are in the same directory: 
\begin{lstlisting}[language=bash,basicstyle=\ttfamily,breaklines=true]
simulator --descriptor-nodes nodetypes.xml --network-file zimmer_boolean_network.txt --experiment-file experiment.xml
\end{lstlisting}
Alternatively, it is also posssible to use the symbols, the previous command would then become:
\begin{lstlisting}[language=bash,basicstyle=\ttfamily,breaklines=true]
simulator -d nodetypes.xml -n zimmer_boolean_network.txt -e experiment.xml
\end{lstlisting}
Finally, it is also possible to define all these parameters in a configuration file and simply using this as the only command line parameter:
\begin{lstlisting}[language=bash,basicstyle=\ttfamily,breaklines=true]
 simulator -c /path/to/configfile
\end{lstlisting}
The content of such a configuration file for the previous example would look like this:
\begin{lstlisting}[language=bash,basicstyle=\ttfamily,breaklines=true]
descriptor-nodes=nodetypes.xml 
network-file=zimmer_boolean_network.txt 
experiment-file=experiment.xml\end{lstlisting}